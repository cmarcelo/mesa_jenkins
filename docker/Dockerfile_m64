FROM otc-mesa-ci.jf.intel.com:5000/debian:trixie-slim

LABEL maintainer="Mesa CI Engineering Team <yuriy.bogdanov@intel.com>" \
    vendor="Mesa Intel" \
    otc-mesa-ci.version="1.3.1-latest" \
    otc-mesa-ci.release-date="2022-07-21" \
    otc-mesa-ci.version.is-production="1.3.1-latest"

ARG DEBIAN_FRONTEND=noninteractive 

# Configure environment
RUN rm -r /etc/apt/sources.list.d

RUN echo 'deb http://linux-ftp.jf.intel.com/pub/mirrors/debian/ testing main contrib non-free' > /etc/apt/sources.list

# runner-packages - x64
RUN apt-get update && \
    apt-get install -y -o"Acquire::http::timeout=7200" \
    asciidoc \
    autoconf \
    avahi-daemon \
    bc \
    bindgen \
    bison \
    build-essential \
    cargo \
    ccache \
    clang-16 \
    cmake \
    cpio \
    debhelper \
    dwarves \
    fakeroot \
    flex \
    g++-multilib \
    gcc-multilib \
    git \
    glslang-tools \
    iputils-ping \
    kmod \
    libc6 \
    libc6-dev \
    libcaca0 \
    libclang-16-dev \
    libclang-cpp16-dev \
    libdrm-dev \
    libdrm2 \
    libedit-dev \
    libegl-mesa0 \
    libegl1-mesa-dev \
    libelf-dev \
    libepoxy-dev \
    libexpat1-dev \
    libgbm-dev \
    libgbm1 \
    libgl1-mesa-dev \
    libgl1-mesa-dri \
    libglapi-mesa \
    libglu1-mesa \
    libglu1-mesa-dev \
    libglx-mesa0 \
    libgtk-3-dev \
    libicu-dev \
    libjpeg-dev \
    libllvmspirvlib-16-dev \
    libllvmspirvlib16 \
    libncurses-dev \
    libomxil-bellagio-dev \
    libosmesa6 \
    libosmesa6-dev \
    libpciaccess-dev \
    libpciaccess0 \
    libpng-dev \
    librust-bindgen-dev \
    librust-tera-dev \
    libsensors5 \
    libssl-dev \
    libtinfo-dev \
    libtool \
    libudev-dev \
    libva-dev \
    libvdpau-dev \
    libvulkan-dev \
    libvulkan1 \
    libwayland-cursor0 \
    libwayland-egl-backend-dev \
    libwayland-egl1-mesa \
    libx11-dev \
    libx11-xcb-dev \
    libxatracker-dev \
    libxatracker2 \
    libxcb-dri2-0-dev \
    libxcb-dri3-dev \
    libxcb-glx0-dev \
    libxcb-present-dev \
    libxcb-randr0-dev \
    libxcb-shm0 \
    libxcb-shm0-dev \
    libxcb-sync-dev \
    libxcb-xfixes0-dev \
    libxdamage-dev \
    libxext-dev \
    libxfixes-dev \
    libxkbcommon0 \
    libxml2-dev \
    libxrandr-dev \
    libxrandr2 \
    libxrender1 \
    libxshmfence-dev \
    libxxf86vm-dev \
    libzstd1 \
    linux-libc-dev \
    mesa-common-dev \
    mesa-opencl-icd \
    mesa-va-drivers \
    mesa-vdpau-drivers \
    mesa-vulkan-drivers \
    meson \
    ninja-build \
    pciutils \
    pkg-config \
    psmisc \
    python3 \
    python3-git \
    python3-lxml \
    python3-mako \
    python3-numpy \
    python3-opencv \
    python3-ply \
    python3-simplejson \
    python3-six \
    python3-yaml \
    rsync \
    rustfmt \
    scons \
    spirv-tools \
    sudo \
    valgrind \
    x11proto-dri2-dev \
    x11proto-gl-dev \
    x11proto-present-dev \
    xserver-xorg \
    xutils-dev \
    xz-utils

# cannot be installed until after other packages are updated (debian bug?)
RUN apt-get update && apt-get install -y \
    libclc-14 && \
    rm -rf /var/lib/apt/lists/*

USER root:root

COPY files/spirv-mesa3d-.spv /usr/lib/clc/
COPY files/spirv64-mesa3d-.spv /usr/lib/clc/

COPY files/Xwrapper.config /etc/X11/Xwrapper.config

RUN chmod u+s /sbin/ldconfig

CMD ["/bin/bash"]

