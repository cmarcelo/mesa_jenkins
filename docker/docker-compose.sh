#!/bin/bash

export RENDER_GROUP=$(getent group render | cut -d: -f3)
export VIDEO_GROUP=$(getent group video | cut -d: -f3)
export USER_ID=$(id -u)
export GROUP_ID=$(id -g)

docker-compose "$@"
